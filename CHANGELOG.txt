
Feeds Test Site 7.x 1.x xxxxxxxxxx
----------------------------------

- Base level upgrade from Drupal 6 to Drupal 7.

Feeds Test Site 6.x 1.6 2010-09-16
----------------------------------

- Add emfield module.
- Upgrade to Feeds 1.0 beta 6.

Feeds Test Site 6.x 1.5 2010-09-11
----------------------------------

- Add Drupal Queue.
- Add Job Scheduler and patches to rely on Job Scheduler to dev make file.
- Upgrade to CTools 1.7 and Data 1.0 Alpha 12.
- Adjust to #851194 Featurize.

Feeds Test Site 6.x 1.4 2010-07-25
----------------------------------

- #851194 alex_b: Featurize. Replace feeds_defaults with feeds_fast_news,
  feeds_import, feeds_news.
- #860272 lyricnz: Upgrade filefield, imagefield to 3.7.
- #860376 lyricnz, alex_b: Don't install some unnecessary modules.
- Remove formatted number module due to conflicts in installation profile (see
  #857928).

Feeds Test Site 6.x 1.3 2010-07-18
----------------------------------

- Upgrade to latest Feeds.
- Add format number API and formatted number module.
- Add schema, dependency of data_ui.

Feeds Test Site 6.x 1.2 2010-06-27
----------------------------------

- Upgrade to latest Feeds.
- Fix problem in rebuild-dev.sh where SimplePie couldn't be downloaded.

Feeds Test Site 6.x 1.1 2010-06-20
----------------------------------

- Roll back to Simpletest 2.9 due to problems with 2.10.

Feeds Test Site 6.x 1.0 2010-06-19
----------------------------------

- Initial release.
