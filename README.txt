
Feeds Test Site
---------------

A Drupal installation profile for testing Feeds module [1]

Installation
------------

If you have not downloaded the full distribution of Feeds Test Site from
drupal.org [2], build the profile at first by running rebuild-dev.sh (see
instructions further below).

The following patches are expected by Feeds to be applied to your Drupal 7
installed codebase:

- Exportability of vocabularies [9]

Once the full site tree is built or downloaded from drupal.org proceed to
install the Feeds Test Site like any other Drupal installation profile [3].

After installing the site you need to do three more steps.

First, download SimplePie [4] and place simplepie.inc in feeds/libraries.

Second, manually install Simpletest following its setup instructions in
INSTALL.txt. You will find simpletest in profiles/feeds_test/modules/simpletest.
It is not automatically installed because it will require you to patch Drupal
core.

Third, if you have not built with rebuild-dev.sh, patch simpletest with
http://drupal.org/files/issues/587304-realpath-D6.patch

Building with rebuild-dev.sh
----------------------------

Using rebuild-dev.sh is recommendable if you develop for Feeds.

Running rebuild-dev.sh requires drush [5] and drush make [6] to be available in
your path. drush make needs to be patched to allow include files [7].

rebuild-dev.sh will check out the latest CVS HEAD of Feeds as working copy -
ideal for diffing patches and testing whether tests complete on the latest
available version of Feeds.

Usage
-----

Navigate to admin/build/testing, check "Feeds" (this will select all tests in
the Feeds group) and run tests.

All tests should be passing.

Learn more about simpletests in its documentation on drupal.org [8].

----

[1] http://drupal.org/project/feeds
[2] http://drupal.org/project/feeds_test
[3] http://drupal.org/getting-started/install
[4] http://simplepie.org/
[5] http://drupal.org/project/drush
[6] http://drupal.org/project/drush_make
[7] http://drupal.org/node/820992
[8] http://drupal.org/node/291740
[9] http://drupal.org/files/issues/881530_vocabulary_field_machine_names.1.patch