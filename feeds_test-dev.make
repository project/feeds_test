; $Id$
; Check out latest HEAD of Feeds.

api = 2

includes[drupal-org] = drupal-org.make

; Pull CTools from devseed's CVS for now
projects[ctools][type] = module
projects[ctools][download][type] = git
projects[ctools][download][url] = git://github.com/developmentseed/ctools.git

projects[date][subdir] = contrib
projects[date][type] = module
projects[date][download][type] = cvs
projects[date][download][module] = contributions/modules/date
projects[date][download][date] = 2010-09-27

; Module implements cache
; http://drupal.org/node/918134#comment-3500152
projects[date][patch][] = http://drupal.org/files/issues/date-load-include-files-earlier.patch

projects[feeds][type] = "module"
projects[feeds][download][type] = "cvs"
projects[feeds][download][module] = "contributions/modules/feeds"
projects[feeds][download][revision] = "DRUPAL-7--2"

; Work with latest CVS version of Job Scheduler
projects[job_scheduler][type] = "module"
projects[job_scheduler][download][type] = "cvs"
projects[job_scheduler][download][module] = "contributions/modules/job_scheduler"
projects[job_scheduler][download][revision] = "HEAD"

projects[views][type] = module
projects[views][download][type] = cvs
projects[views][download][module] = contributions/modules/views
projects[views][download][revision] = DRUPAL-7--3
projects[views][download][date] = 2010-09-20

; Libraries HEAD.
projects[libraries][type] = "module"
projects[libraries][download][type] = "cvs"
projects[libraries][download][module] = "contributions/modules/job_scheduler"
projects[libraries][download][revision] = ":2010-09-15"

; Get Simplepie library.
libraries[simplepie][download][type] = "get"
libraries[simplepie][download][url] = "http://cloud.github.com/downloads/lxbarth/simplepie/SimplePie-1.2-0.tar.gz"
libraries[simplepie][directory_name] = "simplepie"
